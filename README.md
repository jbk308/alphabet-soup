# Welcome to my Alphabet Soup project written in Javascript!<br /><br /> ![](https://media.giphy.com/media/icUEIrjnUuFCWDxFpU/giphy.gif)

<br />

## Step 1
### Initialization of project
Please be sure to run the command listed below before going to the next steps to ensure you have a good package.json file setup. 

```
npm i
```

## Step 2
### In order to run the application and see the output, please run the following command:
```
node wordSearch.js
```

## Result
### The output you should be seeing in your terminal is as follows:
```
HELLO 0:0 4:4
GOOD 4:0 4:3
BYE 1:3 1:1
```
<br /><br />

# Thank you for checking this out! 
![](https://media.giphy.com/media/2rAKTgJIQe1buYU1R5/giphy.gif)
   