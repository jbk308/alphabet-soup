const fs = require('fs');

function parseInput(input) {
  const lines = input.split('\n');
  const gridSize = lines[0].split('x');
  const numRows = parseInt(gridSize[0]);
  const numCols = parseInt(gridSize[1]);
  const grid = lines.slice(1, numRows + 1).map((line) => line.trim().split(' '));
  const words = lines.slice(numRows + 1).map((line) => line.trim()).filter((line) => line !== '');
  return { numRows, numCols, grid, words };
}

function wordFound(word, grid, row, col, dir) {
  const wordLength = word.length;
  const lastRow = row + (dir[0] * (wordLength - 1));
  const lastCol = col + (dir[1] * (wordLength - 1));

  if (lastRow >= 0 && lastRow < grid.length && lastCol >= 0 && lastCol < grid[0].length) {
    for (let i = 0; i < wordLength; i++) {
      const currentRow = row + (dir[0] * i);
      const currentCol = col + (dir[1] * i);
      if (grid[currentRow][currentCol].toLowerCase() !== word[i].toLowerCase()) {
        return false;
      }
    }
    return true;
  }
  return false;
}

function searchWords(grid, words) {
  const directions = [
    [-1, 0],
    [1, 0],
    [0, -1],
    [0, 1],
    [-1, -1],
    [-1, 1],
    [1, -1],
    [1, 1],
  ];

  const results = [];

  for (const word of words) {
    const wordLength = word.length;

    // Iterating over each character in the grid
    for (let row = 0; row < grid.length; row++) {
      for (let col = 0; col < grid[0].length; col++) {
        // Check each direction
        for (const dir of directions) {
          const found = wordFound(word, grid, row, col, dir);
          if (found) {
            const start = `${row}:${col}`;
            const end = `${row + (dir[0] * (wordLength - 1))}:${col + (dir[1] * (wordLength - 1))}`;
            results.push(`${word} ${start} ${end}`);
          }
        }
      }
    }
  }

  return results;
}

fs.readFile('input.txt', 'utf8', (err, data) => {
  if (err) {
    console.error('Error with the input file:', err);
    return;
  }

  const parsedInput = parseInput(data);
  const results = searchWords(parsedInput.grid, parsedInput.words);

  for (const result of results) {
    console.log(result);
  }
});
